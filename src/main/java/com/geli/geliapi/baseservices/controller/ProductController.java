package com.geli.geliapi.baseservices.controller;

import com.geli.geliapi.baseservices.dao.ProductDao;
import com.geli.geliapi.baseservices.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ProductController {

    @Autowired private ProductDao productDao;

    @GetMapping("/api/product")
    public Iterable<Product> allProducts() {

        log.info("Mengambil data produk dari database");
        return productDao.findAll();
    }
}
