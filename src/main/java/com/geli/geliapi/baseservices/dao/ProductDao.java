package com.geli.geliapi.baseservices.dao;

import com.geli.geliapi.baseservices.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
