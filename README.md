## Run Apps
Running bisa menggunakan Maven atau java -jar, untuk jalan di local jangan lupa menambahkan PORT.

Running using Maven command.
```
mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=10005
```

You can also start your Spring Boot application using a java -jar command.

```
mvn package

java -jar target/<FILENAME.JAR HERE> --server.port=10005
```